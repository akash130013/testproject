import React from 'react'
import { useForm } from "react-hook-form";
import axios from 'axios'
import history from '../History'

export default function CreateProduct() {


    const { handleSubmit, register, errors } = useForm();
    const onSubmit = ({name, price,description, quantity}) => {
         
         let data={
             name, price,description, quantity
         }
        const headers = { 
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        };
        axios.post('http://127.0.0.1:8000/api/products',data,headers)
        .then(response => {
            console.log(response);
            history.push('/');
        });

    };

    return (
        <form className="ui form" onSubmit={handleSubmit(onSubmit)}>
            <div className="field">
                <label>Product Name</label>
                <input type="text" name="name" placeholder="Product Name"
                    name="name"
                    ref={register({
                        required: "Required",
                    })}
                />
                {errors.name && errors.name.message}
            </div>
            <div className="field">
                <label>Product Price</label>
                <input type="text" name="price" placeholder="Product Price"
                    ref={register({
                        required: "Required",
                    })}
                />
                {errors.price && errors.price.message}
            </div>
            <div className="field">
                <label>Product Quantity</label>
                <input type="text" name="quantity" placeholder="Product Quantity"
                    ref={register({
                        required: "Required",
                    })}
                />
                {errors.quantity && errors.quantity.message}
            </div>
            <div className="field">
                <label>Product description</label>
                <input type="text" name="description" placeholder="Product description"
                    ref={register({
                        required: "Required",
                    })}
                />

                {errors.description && errors.description.message}
            </div>

            <div className="field">
                <button className="ui button" type="submit">Submit</button>
            </div>
        </form>
    )
}
