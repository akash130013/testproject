import React from 'react'
import {Link} from 'react-router-dom'


export default function Product({ item }) {

  return (
    <div className="item">
      <div className="middle aligned content">
        <a className="header">{item.name}</a>
        <div className="description">
          <p>
            {item.description}
          </p>
        </div>
        <div className="extra">
          <div className="ui right floated button">
            <Link to={`product/${item.id}`}> View</Link>
        </div>
        </div>
      </div>
    </div>

  )
}
