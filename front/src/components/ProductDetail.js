import React from 'react'
import {useState,useEffect} from 'react'
import axios from 'axios'


export default function ProductDetail(props) {

    console.log(props.match.params.id);


    const [detail,setDetail]=useState([]);

      useEffect(() =>{
        const headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        };

        const url=`http://127.0.0.1:8000/api/products/${props.match.params.id}`;
        axios.get(url,headers).then(response=>{
             console.log(response);
             setDetail(response.data.product)
        })

      },[])


    return (
        <div className="ui centered card">
        <div className="content">
            <a className="header">Product Name: {detail.name}</a>
            <div className="description">
               <p>{detail.description}</p>
            </div>
            <div className="ui float left"><h4>Price:$ {detail.price}.00</h4></div>
            <div className="ui float right"><h5>Quantity:{detail.quantity}</h5></div>
        </div>
           
            
      </div>
    )
}
