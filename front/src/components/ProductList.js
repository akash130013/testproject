import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Product from './Product'
import axios from 'axios'


export default class ProductList extends Component {


    state = {
        loading: true,
        data: [],
    }

    componentDidMount() {
        const headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        };
        axios.get('http://127.0.0.1:8000/api/products', headers)
            .then(response => {
                this.setState({
                    data: response.data.products,
                    loading: false,
                })
            });
    }




    render() {

        if (this.state.loading) {
            return <div>loading...</div>

        }


       

        return (
            <div className="">
                  <h2>Product List</h2> 
               
                <Link to="/product/create">
                    <button className="ui secondary button">
                        Add Product
                   </button>
                </Link>
               
               <div className="ui segment">
                <div className="ui relaxed divided list">
                {this.state.data.length>0 &&  this.state.data.map((item, index) => {
                    return <Product key={index} item={item} />;
                })
                }

                {this.state.data.length==0 && <div className="">Product Not Found</div>} 
               </div>
               </div>
            </div>
        )
    }
}
