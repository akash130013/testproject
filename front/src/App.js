import React, { Component } from 'react'
import ProductList from './components/ProductList'
import CreateProduct from './components/CreateProduct'
import { Router, Route, Switch } from 'react-router-dom'
import History from './History'
import Header from './components/Header'
import ProductDetail from './components/ProductDetail'
export default class App extends Component {

    render() {
        return (
            <div className="ui container">
              
                <Router history={History}>
                    <Header />
                    <Switch>
                        <Route path='/' exact={true} component={ProductList} />
                        <Route path='/product/create' component={CreateProduct} />
                        <Route path='/product/:id' component={ProductDetail} />

                    </Switch>
                </Router>

            </div>
        )
    }
}
